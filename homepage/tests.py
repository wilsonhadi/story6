from django.test import TestCase
from django.test.client import Client
from django.apps import apps
from django.urls import resolve
from .models import Status
from .forms import StatusForm
from .views import index
from homepage.apps import HomepageConfig

# Create your tests here.
class test(TestCase):
    def test_application(self):
            self.assertEqual(HomepageConfig.name, 'homepage')
            self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_template_used(self):
            response = Client().get('/')
            self.assertTemplateUsed(response, 'index.html')

    def test_is_url_exist(self):
            response = Client().get('/')
            self.assertEqual(response.status_code,200)

    def test_hello(self):
            response = Client().get('/')
            response_content = response.content.decode('utf-8')
            self.assertIn("Hello", response_content)

    def test_function(self):
            found = resolve('/')
            self.assertEqual(found.func.__name__, index.__name__)

    def test_create_status(self):
            new = Status.objects.create(status = 'test status')
            self.assertTrue(isinstance(new, Status))
            self.assertTrue(new.__str__(), new.status)
            count_status = Status.objects.all().count()
            self.assertEqual(count_status, 1)

    def test_display_status(self):
            status = "test display"
            data = {'stats' : status}
            post_data = Client().post('/', data)
            self.assertEqual(post_data.status_code, 200)

    def test_form_is_valid(self):
            response = self.client.post('', data={'status' : 'Status'})
            response_content = response.content.decode()
            self.assertIn(response_content, 'Status')
